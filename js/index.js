// EXERCISE 1
function sapXep() {
  let so1 = document.getElementById('ex1--so1').value * 1;
  let so2 = document.getElementById('ex1--so2').value * 1;
  let so3 = document.getElementById('ex1--so3').value * 1;
  let result = document.getElementById('sap-xep');

  if (so1 <= so2 && so1 < so3 && so2 <= so3) {
    result.innerHTML = `${so1}, ${so2}, ${so3}`;
  } else if (so1 <= so3 && so1 < so2 && so3 <= so2) {
    result.innerHTML = `${so1}, ${so3}, ${so2}`;
  } else if (so2 <= so3 && so2 < so1 && so3 <= so1) {
    result.innerHTML = `${so2}, ${so3}, ${so1}`;
  } else if (so2 <= so1 && so2 < so3 && so1 <= so3) {
    result.innerHTML = `${so2}, ${so1}, ${so3}`;
  } else if (so3 <= so2 && so3 < so1 && so2 <= so1) {
    result.innerHTML = `${so3}, ${so2}, ${so1}`;
  } else if (so3 <= so1 && so3 < so2 && so1 <= so2) {
    result.innerHTML = `${so3}, ${so1}, ${so2}`;
  } else {
    result.innerHTML = `${so1}, ${so3}, ${so2}`;
  }
}

// EXERCISE 2
function sayHello() {
  let member = document.getElementById('member').value;

  if (member == 'C') {
    document.getElementById('xin-chao').innerText = 'Xin chào Chị!';
  } else if (member == 'B') {
    document.getElementById('xin-chao').innerText = 'Xin chào Bố!';
  } else if (member == 'M') {
    document.getElementById('xin-chao').innerText = 'Xin chào Mẹ!';
  } else if (member == 'E') {
    document.getElementById('xin-chao').innerText = 'Xin chào Em!';
  }
}

// EXERCISE 3
function counterParity() {
  let so1 = document.getElementById('ex3--so1').value * 1;
  let so2 = document.getElementById('ex3--so2').value * 1;
  let so3 = document.getElementById('ex3--so3').value * 1;
  let count = 0;

  if (so1 % 2 == 0) {
    count++;
  }
  if (so2 % 2 == 0) {
    count++;
  }
  if (so3 % 2 == 0) {
    count++;
  }
  document.getElementById('chan').innerHTML = `${count} số chẵn`;
  document.getElementById('le').innerHTML = `${3 - count} số lẻ`;
}

// EXERCISE 4
function guessTheTriangle() {
  let canh1 = document.getElementById('canh1').value * 1;
  let canh2 = document.getElementById('canh2').value * 1;
  let canh3 = document.getElementById('canh3').value * 1;
  let result = document.getElementById('triangle');
  if (canh1 + canh2 > canh3 || canh1 + canh3 > canh2 || canh2 + canh3 > canh1) {
    if (canh1 === canh2 && canh2 === canh3) {
      result.innerHTML = `Đây là tam giác đều.`;
    } else if (canh1 === canh2 || canh1 === canh3 || canh2 === canh3) {
      result.innerHTML = `Đây là tam giác cân.`;
    } else if (
      Math.pow(canh1, 2) === Math.pow(canh2, 2) + Math.pow(canh3, 2) ||
      Math.pow(canh2, 2) === Math.pow(canh1, 2) + Math.pow(canh3, 2) ||
      Math.pow(canh3, 2) === Math.pow(canh2, 2) + Math.pow(canh1, 2)
    ) {
      result.innerHTML = `Đây là tam giác vuông.`;
    } else {
      result.innerHTML = `Đây là tam giác thường.`;
    }
  } else {
    arlet('Đây không phải là một tam giác. Vui lòng nhập đúng độ dài cạnh tam giác!!');
  }
}
